package bzu.comp.os.project.driver;

import bzu.comp.os.project.service.SchedulerIteratorService;

/**
 * This class servers as the driver class to print the outputs
 * @author Tawfiq Abdulraziq
 */
public class Driver {

    public static void main(String[] args) {

        System.out.println();
        System.out.println("Operating Systems Project - 2019");
        System.out.println("Copyright © 2019 Tawfiq Abdulraziq");
        System.out.println();

        SchedulerIteratorService.printTimingsForFirstComeFirstServe();
        SchedulerIteratorService.printTimingsForShortestJobFirst();
        SchedulerIteratorService.printTimingsForRoundRobin();
        SchedulerIteratorService.printTimingsForMultiLevelFeedback();


    }
}
