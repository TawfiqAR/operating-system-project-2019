package bzu.comp.os.project.entity;

/**
 * This class is an entity class that represents each job (process) that is ready to be executed
 * @author Tawfiq Abdulraziq
 */
public class Job {

    private int processId;
    private int arrivalTime;
    private int burstTime;
    private int waitingTime;
    private int turnAroundTime;

    public Job(){
        // Default Constructor
    }

    public int getProcessId() {
        return processId;
    }

    @Override
    public String toString() {
        return "Job{" +
                "processId=" + processId +
                ", arrivalTime=" + arrivalTime +
                ", burstTime=" + burstTime +
                ", waitingTime=" + waitingTime +
                ", turnAroundTime=" + turnAroundTime +
                '}';
    }

    public void setProcessId(int processId) {
        this.processId = processId;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(int burstTime) {
        this.burstTime = burstTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int getTurnAroundTime() {
        return turnAroundTime;
    }

    public void setTurnAroundTime(int turnAroundTime) {
        this.turnAroundTime = turnAroundTime;
    }
}
