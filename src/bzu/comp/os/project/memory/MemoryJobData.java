package bzu.comp.os.project.memory;

import bzu.comp.os.project.entity.Job;

import java.util.ArrayList;
import java.util.List;

/**
 * The class contains in memory data for testing purposes
 * @author Tawfiq Abdulraziq
 */
public class MemoryJobData {

    List<Job> jobs = new ArrayList<>();

    public MemoryJobData(){
        Job job1 = new Job();
        job1.setProcessId(1);
        job1.setArrivalTime(0);
        job1.setBurstTime(10);

        Job job2 = new Job();
        job2.setProcessId(2);
        job2.setArrivalTime(0);
        job2.setBurstTime(5);

        Job job3 = new Job();
        job3.setProcessId(3);
        job3.setArrivalTime(0);
        job3.setBurstTime(8);


        jobs.add(job1);
        jobs.add(job2);
        jobs.add(job3);
    }

    public List<Job> findAll(){
        return jobs;
    }
}
