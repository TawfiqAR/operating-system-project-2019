package bzu.comp.os.project.constant;

/**
 * This class is a final class that includes necessary constants for the program
 * @author Tawfiq Abdulraziq
 */
public final class JobConstant {
    public static final int ROUND_ROBIN_QUANTUM = 50;
    public static final int MULTI_LEVEL_FIRST_QUEUE_QUANTUM = 20;
    public static final int MULTI_LEVEL_SECOND_QUEUE_QUANTUM = 100;

    public static final int LOWEST_POSSIBLE_ARRIVAL_TIME = 0;
    public static final int HIGHEST_POSSIBLE_ARRIVAL_TIME = 100;

    public static final int LOWEST_POSSIBLE_BURST_TIME = 10;
    public static final int HIGHEST_POSSIBLE_BURST_TIME = 1000;

    private JobConstant() {
        // Default constructor
    }
}
