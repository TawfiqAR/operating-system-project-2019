package bzu.comp.os.project.service;

import bzu.comp.os.project.constant.JobConstant;
import bzu.comp.os.project.entity.Job;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class JobCreation {

    private JobCreation(){
        // Default constructor
    }

    public static List<Job> createJobs(int numberOfJobs){
        List<Job> jobs = new ArrayList<>();
        Random randomNumber = new Random();
        for (int j = 0; j < numberOfJobs; j++) {
            Job job = new Job();
            job.setProcessId(j);
            job.setArrivalTime(randomNumber.nextInt(JobConstant.HIGHEST_POSSIBLE_ARRIVAL_TIME - JobConstant.LOWEST_POSSIBLE_ARRIVAL_TIME) + JobConstant.LOWEST_POSSIBLE_ARRIVAL_TIME);
            job.setBurstTime(randomNumber.nextInt(JobConstant.HIGHEST_POSSIBLE_BURST_TIME - JobConstant.LOWEST_POSSIBLE_BURST_TIME) + JobConstant.LOWEST_POSSIBLE_BURST_TIME);
            jobs.add(job);
        }
        return jobs;
    }
}
