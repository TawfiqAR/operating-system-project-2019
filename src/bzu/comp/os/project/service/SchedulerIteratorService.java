package bzu.comp.os.project.service;

import bzu.comp.os.project.scheduler.FirstComeFirstServe;
import bzu.comp.os.project.scheduler.MultiLevelFeedback;
import bzu.comp.os.project.scheduler.RoundRobin;
import bzu.comp.os.project.scheduler.ShortestJobFirst;

import java.util.List;

/**
 * This class serves to print all the outcomes for all the schedulers
 * @author Tawfiq Abdulraziq
 */
public final class SchedulerIteratorService {

    private SchedulerIteratorService() {
        // Default constructor
    }

    /**
     * Print the outcomes for First Come First Serve scheduler
     * @author Tawfiq Abdulraziq
     */
    public static void printTimingsForFirstComeFirstServe() {
        FirstComeFirstServe firstComeFirstServe = new FirstComeFirstServe();
        List<Double> iterations10 = firstComeFirstServe.getAverageTimingsWithIterations(100);
        List<Double> iterations100 = firstComeFirstServe.getAverageTimingsWithIterations(1000);
        List<Double> iterations1000 = firstComeFirstServe.getAverageTimingsWithIterations(10000);
        List<Double> iterations10000 = firstComeFirstServe.getAverageTimingsWithIterations(100000);

        System.out.println("First Come First Serve");
        System.out.println("------");
        System.out.format("%-32s%-32s%-32s%-32s%s\n", "Iterations", "100", "1000", "10000", "100000");
        System.out.format("%-32s%-32.2f%-32.2f%-32.2f%.2f\n", "AWT",
                iterations10.get(0),
                iterations100.get(0),
                iterations1000.get(0),
                iterations10000.get(0));
        System.out.format("%-32s%-32.2f%-32.2f%-32.2f%.2f\n", "ATT",
                iterations10.get(1),
                iterations100.get(1),
                iterations1000.get(1),
                iterations1000.get(1));

        System.out.println();
    }

    /**
     * Print the outcomes for Shortest Job scheduler
     * @author Tawfiq Abdulraziq
     */
    public static void printTimingsForShortestJobFirst() {
        ShortestJobFirst shortestJobFirst = new ShortestJobFirst();
        List<Double> iterations10 = shortestJobFirst.getAverageTimingsWithIterations(100);
        List<Double> iterations100 = shortestJobFirst.getAverageTimingsWithIterations(1000);
        List<Double> iterations1000 = shortestJobFirst.getAverageTimingsWithIterations(10000);
        List<Double> iterations10000 = shortestJobFirst.getAverageTimingsWithIterations(100000);

        System.out.println("Shortest Job First");
        System.out.println("------");
        System.out.format("%-32s%-32s%-32s%-32s%s\n", "Iterations", "100", "1000", "10000", "100000");
        System.out.format("%-32s%-32.2f%-32.2f%-32.2f%.2f\n", "AWT",
                iterations10.get(0),
                iterations100.get(0),
                iterations1000.get(0),
                iterations10000.get(0));
        System.out.format("%-32s%-32.2f%-32.2f%-32.2f%.2f\n", "ATT",
                iterations10.get(1),
                iterations100.get(1),
                iterations1000.get(1),
                iterations1000.get(1));

        System.out.println();
    }

    /**
     * Print the outcomes for Round Robin scheduler
     * @author Tawfiq Abdulraziq
     */
    public static void printTimingsForRoundRobin() {
        RoundRobin roundRobin = new RoundRobin();
        List<Double> iterations10 = roundRobin.getAverageTimingsWithIterations(100);
        List<Double> iterations100 = roundRobin.getAverageTimingsWithIterations(1000);
        List<Double> iterations1000 = roundRobin.getAverageTimingsWithIterations(10000);
        List<Double> iterations10000 = roundRobin.getAverageTimingsWithIterations(100000);

        System.out.println("Round Robin");
        System.out.println("------");
        System.out.format("%-32s%-32s%-32s%-32s%s\n", "Iterations", "100", "1000", "10000", "100000");
        System.out.format("%-32s%-32.2f%-32.2f%-32.2f%.2f\n", "AWT",
                iterations10.get(0),
                iterations100.get(0),
                iterations1000.get(0),
                iterations10000.get(0));
        System.out.format("%-32s%-32.2f%-32.2f%-32.2f%.2f\n", "ATT",
                iterations10.get(1),
                iterations100.get(1),
                iterations1000.get(1),
                iterations1000.get(1));

        System.out.println();
    }

    /**
     * Print the outcomes for Multi-Level Feedback Queue scheduler
     * @author Tawfiq Abdulraziq
     */
    public static void printTimingsForMultiLevelFeedback() {
        MultiLevelFeedback multiLevelFeedback = new MultiLevelFeedback();
        List<Double> iterations10 = multiLevelFeedback.getAverageTimingsWithIterations(100);
        List<Double> iterations100 = multiLevelFeedback.getAverageTimingsWithIterations(1000);
        List<Double> iterations1000 = multiLevelFeedback.getAverageTimingsWithIterations(10000);
        List<Double> iterations10000 = multiLevelFeedback.getAverageTimingsWithIterations(100000);

        System.out.println("Multi Level Feedback Queue");
        System.out.println("------");
        System.out.format("%-32s%-32s%-32s%-32s%s\n", "Iterations", "100", "1000", "10000", "100000");
        System.out.format("%-32s%-32.2f%-32.2f%-32.2f%.2f\n", "AWT",
                iterations10.get(0),
                iterations100.get(0),
                iterations1000.get(0),
                iterations10000.get(0));
        System.out.format("%-32s%-32.2f%-32.2f%-32.2f%.2f\n", "ATT",
                iterations10.get(1),
                iterations100.get(1),
                iterations1000.get(1),
                iterations1000.get(1));

        System.out.println();
    }
}
