package bzu.comp.os.project.scheduler;

import bzu.comp.os.project.entity.Job;
import bzu.comp.os.project.service.JobCreation;

import java.util.*;

/**
 * This class is a responsible for creating and processing a Shortest Job First (without preemption) scheduler
 * @author Tawfiq Abdulraziq
 */
public class ShortestJobFirst implements Scheduler {

    private List<Job> jobList;
    private List<Job> readyJobs;
    private int totalTime;

    public ShortestJobFirst() {
        initialize();
    }

    @Override
    public void initialize() {
        jobList = new ArrayList<>();
        readyJobs = new ArrayList<>();
        totalTime = 0;
    }

    @Override
    public void loadJobs(List<Job> jobs) {
        jobList = jobs;
    }

    @Override
    public void serveJobs() {
        jobList.sort(Comparator.comparing(Job::getArrivalTime));
        List<Job> arrivedJobs = new ArrayList<>();

        while (jobList.size() > 0) {
            arrivedJobs.clear();
            jobList.forEach((job) -> {
                if (job.getArrivalTime() <= totalTime) {
                    arrivedJobs.add(job);
                }
            });

            if (arrivedJobs.isEmpty()) {
                serveFirstNotArrivedJob();
            } else {
                arrivedJobs.sort(Comparator.comparing(Job::getBurstTime));
                serveArrivedJobs(arrivedJobs);
            }
        }
    }

    @Override
    public double getAverageWaitingTime() {
        double totalWaitingTime = readyJobs.stream().mapToInt(Job::getWaitingTime).sum();
        return totalWaitingTime / readyJobs.size();
    }

    @Override
    public double getAverageTurnAroundTime() {
        double totalTurnAroundTime = readyJobs.stream().mapToInt(Job::getTurnAroundTime).sum();
        return totalTurnAroundTime / readyJobs.size();
    }

    @Override
    public List<Double> getAverageTimingsWithIterations(int iterations) {
        double totalWaitingTime = 0;
        double totalTurnAroundTime = 0;
        for (int i = 0; i < iterations; i++) {
            initialize();
            loadJobs(JobCreation.createJobs(12));
            serveJobs();
            totalWaitingTime += getAverageWaitingTime();
            totalTurnAroundTime += getAverageTurnAroundTime();
        }
        totalWaitingTime /= iterations;
        totalTurnAroundTime /= iterations;
        return (Arrays.asList(totalWaitingTime,totalTurnAroundTime));
    }


    /**
     * Find the next job to execute when the arrived jobs list is empty
     *
     * @author Tawfiq Abdulraziq
     */
    private void serveFirstNotArrivedJob() {
        totalTime += jobList.get(0).getArrivalTime() - totalTime;
        jobList.get(0).setWaitingTime(0);
        totalTime += jobList.get(0).getBurstTime();
        jobList.get(0).setTurnAroundTime(jobList.get(0).getWaitingTime() + jobList.get(0).getBurstTime());
        readyJobs.add(jobList.get(0));
        jobList.remove(jobList.get(0));
    }

    /**
     * Serve the arrived jobs
     *
     * @author Tawfiq Abdulraziq
     */
    private void serveArrivedJobs(List<Job> arrivedJobs){
        arrivedJobs.forEach((arrivedJob) -> {
            arrivedJob.setWaitingTime(totalTime - arrivedJob.getArrivalTime());
            totalTime += arrivedJob.getBurstTime();
            arrivedJob.setTurnAroundTime(arrivedJob.getWaitingTime() + arrivedJob.getBurstTime());
            readyJobs.add(arrivedJob);
            jobList.remove(arrivedJob);
        });
    }

    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public List<Job> getReadyJobs() {
        return readyJobs;
    }

    public void setReadyJobs(List<Job> readyJobs) {
        this.readyJobs = readyJobs;
    }
}
