package bzu.comp.os.project.scheduler;

import bzu.comp.os.project.entity.Job;

import java.util.List;

/**
 * This interface contains all the necessary methods for all schedulers
 *
 * @author Tawfiq Abdulraziq
 */
public interface Scheduler {
    /**
     * Initialize data
     * @author Tawfiq Abdulraziq
     */
    void initialize();
    /**
     * Load the list of jobs into the queue
     * @author Tawfiq Abdulraziq
     * @param jobs
     */
    void loadJobs(List<Job> jobs);

    /**
     * Serve the jobs in the queue
     * @author Tawfiq Abdulraziq
     */
    void serveJobs();

    /**
     * Calculate the average waiting time of the scheduling algorithm at the given processes
     * @author Tawfiq Abdulraziq
     * @return average waiting time
     */
    double getAverageWaitingTime();

    /**
     * Calculate the average turn around time of the scheduling algorithm at the given processes
     * @author Tawfiq Abdulraziq
     * @return average turn around time
     */
    double getAverageTurnAroundTime();

    /**
     * Get the waiting time and turn around time after the given number of iterations
     * @author Tawfiq Abdulraziq
     * @param iterations
     * @return list containing the average waiting time and the average turn around time
     */
    List<Double> getAverageTimingsWithIterations(int iterations);



}
