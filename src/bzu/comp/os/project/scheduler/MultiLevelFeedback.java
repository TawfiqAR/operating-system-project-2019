package bzu.comp.os.project.scheduler;

import bzu.comp.os.project.constant.JobConstant;
import bzu.comp.os.project.entity.Job;
import bzu.comp.os.project.service.JobCreation;

import java.util.*;

/**
 * This class is a responsible for creating and processing a Multi-Level Feedback Queue scheduler
 *
 * @author Tawfiq Abdulraziq
 */
public class MultiLevelFeedback implements Scheduler {

    private List<Job> jobList;
    private List<Job> readyJobs;
    private List<Job> tempJobs;
    private int totalTime;
    private int firstRoundRobinQuantum;
    private int secondRoundRobinQuantum;

    public MultiLevelFeedback() {
        initialize();
    }

    /**
     * Initialize data
     *
     * @author Tawfiq Abdulraziq
     */
    @Override
    public void initialize() {
        jobList = new ArrayList<>();
        readyJobs = new ArrayList<>();
        tempJobs = new ArrayList<>();
        totalTime = 0;
        firstRoundRobinQuantum = JobConstant.MULTI_LEVEL_FIRST_QUEUE_QUANTUM;
        secondRoundRobinQuantum = JobConstant.MULTI_LEVEL_SECOND_QUEUE_QUANTUM;
    }

    @Override
    public void loadJobs(List<Job> jobs) {
        jobList = jobs;
        tempJobs.addAll(jobs);
    }

    @Override
    public void serveJobs() {
        jobList.sort(Comparator.comparing(Job::getArrivalTime));
        List<Job> arrivedJobs = new ArrayList<>();
        while (jobList.size() > 0) {
            arrivedJobs.clear();
            jobList.forEach((job) -> {
                if (job.getArrivalTime() <= totalTime) {
                    arrivedJobs.add(job);
                }
            });

            if (arrivedJobs.isEmpty()) {
                serveFirstNotArrivedJob();
            } else {
                serveArrivedJobsInFirstQueue(arrivedJobs);
            }
            if (!arrivedJobs.isEmpty())
                serveArrivedJobsInSecondQueue(arrivedJobs);
            }
            if (!arrivedJobs.isEmpty()) {
                serveArrivedJobsInThirdQueue(arrivedJobs);
            }
    }

    @Override
    public double getAverageWaitingTime() {
        double totalWaitingTime = readyJobs.stream().mapToInt(Job::getWaitingTime).sum();
        return totalWaitingTime / readyJobs.size();
    }

    @Override
    public double getAverageTurnAroundTime() {
        double totalWaitingTime = readyJobs.stream().mapToInt(Job::getTurnAroundTime).sum();
        return totalWaitingTime / readyJobs.size();
    }

    @Override
    public List<Double> getAverageTimingsWithIterations(int iterations) {
        double totalWaitingTime = 0;
        double totalTurnAroundTime = 0;
        for (int i = 0; i < iterations; i++) {
            initialize();
            loadJobs(JobCreation.createJobs(12));
            serveJobs();
            totalWaitingTime += getAverageWaitingTime();
            totalTurnAroundTime += getAverageTurnAroundTime();
        }
        totalWaitingTime /= iterations;
        totalTurnAroundTime /= iterations;
        return (Arrays.asList(totalWaitingTime,totalTurnAroundTime));
    }

    /**
     * Find the original job in the job list through it's id
     *
     * @param selectedJob
     * @return job
     * @author Tawfiq Abdulraziq
     */
    private Job findById(Job selectedJob) {
        return (tempJobs.stream().
                filter(job -> job.getProcessId() == selectedJob.getProcessId()).
                findAny().orElse(null));
    }

    /**
     * Remove the selected job from the job list
     *
     * @param arrivedJob
     * @author Tawfiq Abdulraziq
     */
    private void removeJob(Job arrivedJob) {
        totalTime += arrivedJob.getBurstTime();
        int originalBurst = findById(arrivedJob).getBurstTime();
        arrivedJob.setWaitingTime(totalTime - originalBurst);
        arrivedJob.setTurnAroundTime(originalBurst + arrivedJob.getWaitingTime());
        arrivedJob.setBurstTime(originalBurst);
        readyJobs.add(arrivedJob);
        jobList.remove(arrivedJob);
    }

    /**
     * Find the next job to execute when the arrived jobs list is empty in first queue
     * @author Tawfiq Abdulraziq
     */
    private void serveFirstNotArrivedJob() {
        totalTime += jobList.get(0).getArrivalTime() - totalTime;
        jobList.get(0).setWaitingTime(0);
        totalTime += firstRoundRobinQuantum;
        jobList.get(0).setBurstTime(jobList.get(0).getBurstTime() - firstRoundRobinQuantum);
        if (jobList.get(0).getBurstTime() <= 0) {
            removeJob(jobList.get(0));
        }
    }

    /**
     * Serve the arrived jobs in the first round robin queue
     * @author Tawfiq Abdulraziq
     */
    private void serveArrivedJobsInFirstQueue(List<Job> arrivedJobs){
        arrivedJobs.forEach((arrivedJob) -> {
            totalTime += firstRoundRobinQuantum;
            arrivedJob.setBurstTime(arrivedJob.getBurstTime() - firstRoundRobinQuantum);
            if (arrivedJob.getBurstTime() <= 0) {
                removeJob(arrivedJob);
            }
        });
    }

    /**
     * Serve the arrived jobs in the second round robin queue
     * @author Tawfiq Abdulraziq
     */
    private void serveArrivedJobsInSecondQueue(List<Job> arrivedJobs){
        arrivedJobs.forEach((arrivedJob) -> {
            totalTime += secondRoundRobinQuantum;
            arrivedJob.setBurstTime(arrivedJob.getBurstTime() - secondRoundRobinQuantum);
            if (arrivedJob.getBurstTime() <= 0) {
                removeJob(arrivedJob);
            }
        });
    }

    /**
     * Serve the arrived jobs in the third first come first serve queue
     * @author Tawfiq Abdulraziq
     */
    private void serveArrivedJobsInThirdQueue(List<Job> arrivedJobs){
        arrivedJobs.stream().forEach((arrivedJob) -> {
            arrivedJob.setWaitingTime(totalTime);
            arrivedJob.setTurnAroundTime(findById(arrivedJob).getBurstTime() + arrivedJob.getWaitingTime());
            totalTime += arrivedJob.getBurstTime();
            arrivedJob.setBurstTime(findById(arrivedJob).getBurstTime());
            readyJobs.add(arrivedJob);
            jobList.remove(arrivedJob);
        });
    }


    public List<Job> getJobList() {
        return jobList;
    }

    public void setJobList(List<Job> jobList) {
        this.jobList = jobList;
    }

    public List<Job> getReadyJobs() {
        return readyJobs;
    }

    public void setReadyJobs(List<Job> readyJobs) {
        this.readyJobs = readyJobs;
    }

    public List<Job> getTempJobs() {
        return tempJobs;
    }

    public void setTempJobs(List<Job> tempJobs) {
        this.tempJobs = tempJobs;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public int getFirstRoundRobinQuantum() {
        return firstRoundRobinQuantum;
    }

    public void setFirstRoundRobinQuantum(int firstRoundRobinQuantum) {
        this.firstRoundRobinQuantum = firstRoundRobinQuantum;
    }

    public int getSecondRoundRobinQuantum() {
        return secondRoundRobinQuantum;
    }

    public void setSecondRoundRobinQuantum(int secondRoundRobinQuantum) {
        this.secondRoundRobinQuantum = secondRoundRobinQuantum;
    }
}
